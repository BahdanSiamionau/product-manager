<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
	<head>
		<title>Adding new good</title>
		<script type="text/javascript">
    		function checkBoxToggled(){
    			var checkbox = document.getElementById("cb");
    			var priceText = document.getElementById("pt");
		        if(checkbox.checked) {
		        	priceText.enabled=false;
		        	priceText.value="";
		        	priceText.style.backgroundColor="#b0bca7";
		        } else {
		        	priceText.enabled=true;
		        	priceText.style.backgroundColor="white";
		        }
		    }
		</script>
	</head>
	<body onLoad="checkBoxToggled()">
		<html:link action="/ProductAction.do?method=categories">
			Categories
		</html:link>
		&#062;&#062;
		<html:link action="/ProductAction.do?method=subcategories&amp;category=${category}">
			${category}
		</html:link>
		&#062;&#062; 
		<html:link action="/ProductAction.do?method=goods&amp;category=${category}&amp;subcategory=${subcategory}">
			${subcategory}
		</html:link>
		&#062;&#062; Add
		<br/>
		<br/>
		<nested:form action="/ProductAction.do?method=save" method="post">
			<table>
				<tr>
					<td width="100px">Producer</td>
					<td>
						<input type="text" name="producer"/>
					</td>
				</tr>
				<tr>
					<td width="100px">Model</td>
					<td>
						<input type="text" name="model"/>
					</td>
				</tr>
				<tr>
					<td width="100px">Date of issue</td>
					<td>
						<input type="text" name="date"/>
					</td>
				</tr>
				<tr>
					<td width="100px">Color</td>
					<td>
						<input type="text" name="color"/>
					</td>
				</tr>
				<tr>
					<td>Price</td>
					<td>
						<input type="text" name="price" id="pt"/>
					</td>
				</tr>
				<tr>
					<td>
						<input name="notInStock" type="checkbox" id="cb" 
							onClick="checkBoxToggled()"/>
						Good is not in stock
					</td>
				</tr>
			</table>
			<nested:hidden property="category" value="${category}"/>
			<nested:hidden property="subcategory" value="${subcategory}"/>
			<nested:submit style="font-size: 18px; float: left; margin-top: 19px; background-color: #b0bca7;" value="save"/>
		</nested:form>
		<nested:form action="/ProductAction.do?method=goods" method="post">
			<nested:hidden property="category" value="${category}"/>
			<nested:hidden property="subcategory" value="${subcategory}"/>
			<nested:submit style="font-size: 18px; margin-left: 50px; background-color: #b0bca7;" value="cancel"/>
		</nested:form>
	</body>
</html>