<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
<head>
	<title>Goods</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<script type="text/javascript">
   		function checkBoxToggled(id, priceName){
   			var checkbox = document.getElementById(id);
   			var elements = document.getElementsByName(priceName);
   			var priceText = elements[0];
	        if(checkbox.checked) {
	        	priceText.value="";
	        	priceText.style.backgroundColor="#b0bca7";
	        	priceText.readOnly=true;
	        } else {
	        	priceText.style.backgroundColor="white";
	        	priceText.readOnly=false;
	        }
	    }
	</script>
</head>
<body>
	<nested:root name="productForm">
		<html:link action="/ProductAction.do?method=categories">
			Categories
		</html:link>
		&#062;&#062;
		<html:link action="/ProductAction.do?method=subcategories&amp;category=${category}">
			${category}
		</html:link>
		&#062;&#062; ${subcategory}
		<br/>
		<br/>
		<nested:form action="/ProductAction.do?method=update&amp;category=${category}&amp;subcategory=${subcategory}" method="post">
			<table>
				<tr style="background-color: #b0bca7;">
					<td width="100px">Title</td>
					<td width="100px">Model</td>
					<td width="100px">Date of issue</td>
					<td width="100px">Color</td>
					<td width="100px">Price</td>
					<td>Not in stock</td>
				</tr>
				<nested:nest property="document.rootElement">
					<nested:iterate property="children[${categoryId}].children[${subcategoryId}].children" indexId="id">
						<tr>
							<td>
								<nested:text property="children[0].text"/>
							</td>
							<td>
								<nested:text property="children[1].text"/>
							</td>
							<td>
								<nested:text property="children[2].text"/>
							</td>
							<td>
								<nested:text property="children[3].text"/>
							</td>
							<td>
								<nested:text property="children[4].text"/>
							</td>
							<td>
								<nested:empty property="children[4].text">
									<input type="checkbox" checked="checked" onClick="checkBoxToggled('cb${id}', 'document.rootElement.children[${categoryId}].children[${subcategoryId}].children[${id}].children[4].text')" id="cb${id}"/>
								</nested:empty>
								<nested:notEmpty property="children[4].text">
									<input type="checkbox"  onClick="checkBoxToggled('cb${id}', 'document.rootElement.children[${categoryId}].children[${subcategoryId}].children[${id}].children[4].text')" id="cb${id}"/>
								</nested:notEmpty>
								<script type="text/javascript">
									checkBoxToggled('cb${id}', 'document.rootElement.children[${categoryId}].children[${subcategoryId}].children[${id}].children[4].text');
								</script>
							</td>
						</tr>
					</nested:iterate>
				</nested:nest>
			</table>
			<nested:submit style="font-size: 18px; float: left; margin-top: 19px; background-color: #b0bca7;" value="update"/>
		</nested:form>
		<nested:form action="/ProductAction.do?method=add" method="post">
			<nested:hidden property="category" value="${category}"/>
			<nested:hidden property="subcategory" value="${subcategory}"/>
			<nested:submit style="font-size: 18px; background-color: #b0bca7; margin-left: 50px;" value="add"/>
		</nested:form>
	</nested:root>
</body>
</html>