<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<html>
<head>
	<title>Categories</title>
</head>
<body>
	Categories
  	<br/>
  	<br/>
	<table>
		<nested:root name="productForm">
			<nested:iterate property="document.rootElement.children">
				<tr>
					<td width="100px">
						<nested:define id="category" property="attributes[0].value"/>
						<html:link action="/ProductAction.do?method=subcategories&category=${category}" >
							${category}
						</html:link>
					</td>
					<td>
						<bean:parameter id="size" name="size" value="0"/>
						<nested:iterate property="children">
							<nested:size id="temp" property="children"/>
							<bean:parameter id="size" name="size" value="${size + temp}"/>
						</nested:iterate>
						${size}
					</td>
				</tr>
			</nested:iterate>
		</nested:root>
	</table>
</body>
</html>