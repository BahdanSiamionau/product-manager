<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
<head>
	<title>Subcategories</title>
</head>
<body>
	<nested:root name="productForm">
		<html:link action="/ProductAction.do?method=categories">
			Categories
		</html:link>
		&#062;&#062; ${category}
		<br/>
		<br/>
		<table>
			<nested:nest property="document.rootElement">
				<nested:iterate property="children[${categoryId}].children">
					<tr>
						<td width="100px">
							<nested:define id="subcategory" property="attributes[0].value"/>
							<html:link action="/ProductAction.do?method=goods&category=${category}&subcategory=${subcategory}">
								${subcategory}
							</html:link>
						</td>
						<td>
							<nested:size id="size" property="children"/>
							${size}
						</td>
					</tr>
				</nested:iterate>
			</nested:nest>
		</table>
	</nested:root>
</body>
</html>