<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:validator="xalan://com.epam.xsltransformations.validation.InputValidator"
				xmlns:result="xalan://com.epam.xsltransformations.validation.ValidationResult"
				xmlns:good="xalan://com.epam.xsltransformations.model.Good"
            	version="1.0">
    
    <xsl:param name="category"/>            
    <xsl:param name="subcategory"/>
    <xsl:param name="producer"/>
    <xsl:param name="model"/>
    <xsl:param name="date"/>
    <xsl:param name="color"/>
    <xsl:param name="price"/>
    
    <xsl:template match="@* | node()">
    	<xsl:copy>
       		<xsl:apply-templates select="@* | node()"/>
    	</xsl:copy>
	</xsl:template>
	
	<xsl:template match="Products/Category[@name=$category]/Subcategory[@name=$subcategory]">
   		<xsl:copy>
        	<xsl:apply-templates select="@* | node()"/>
       		<xsl:element name="Good">
      			<xsl:element name="Producer">
					<xsl:value-of select="$producer"/>
				</xsl:element>
				<xsl:element name="Model">
					<xsl:value-of select="$model"/>
				</xsl:element>
				<xsl:element name="Date-of-issue">
					<xsl:value-of select="$date"/>
				</xsl:element>
				<xsl:element name="Color">
					<xsl:value-of select="$color"/>
				</xsl:element>
				<xsl:choose>
					<xsl:when test="$price">
						<xsl:element name="Price">
							<xsl:value-of select="$price"/>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="Not-in-stock"/>
					</xsl:otherwise>
				</xsl:choose>
       		</xsl:element>
    	</xsl:copy>
	</xsl:template>

</xsl:stylesheet>