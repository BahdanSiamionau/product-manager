package com.epam.productmanager.resource;

import java.util.ResourceBundle;

public final class ManagerBundle {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(ManagerConstants.PATH);
	
	private ManagerBundle() {
		
	}
	
	public static String getString(String key) {
		return BUNDLE.getString(key);
	}
}