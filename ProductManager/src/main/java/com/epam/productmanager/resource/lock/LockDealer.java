package com.epam.productmanager.resource.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class LockDealer {

	private static final class InstanceHolder {
		private static final ReentrantReadWriteLock instance = new ReentrantReadWriteLock(); 
	}
	
	private static ReentrantReadWriteLock getInstance() {
		return InstanceHolder.instance;
	}
	
	private LockDealer() {
		
	}
	
	public static ReentrantReadWriteLock getReadWriteLock() {
		return getInstance();
	}
}
