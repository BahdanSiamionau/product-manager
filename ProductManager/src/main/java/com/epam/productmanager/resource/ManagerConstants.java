package com.epam.productmanager.resource;

public final class ManagerConstants {

	public static final String PRODUCER = "producer";
	public static final String MODEL = "model";
	public static final String DATE = "date";
	public static final String COLOR = "color";
	public static final String PRICE = "price";
	public static final String NOT_IN_STOCK = "notInStock";
	public static final String PATH = "path";
	public static final String XSL_SAVE = "xsl.save";
	public static final String XML_PATH = "xml";
	public static final String CATEGORY = "category";
	public static final String SUBCATEGORY = "subcategory";
	public static final String CATEGORIES = "categories";
	public static final String SUBCATEGORIES = "subcategories";
	public static final String NAME = "name";
	public static final String CATEGORY_ID = "categoryId";
	public static final String SUBCATEGORY_ID = "subcategoryId";
	public static final String GOODS = "goods";
	public static final String ADD = "add";
	
	private ManagerConstants() {
		
	}
}
