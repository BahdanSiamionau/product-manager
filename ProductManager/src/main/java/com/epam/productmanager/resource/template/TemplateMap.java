package com.epam.productmanager.resource.template;

import java.io.File;
import java.util.HashMap;

import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.epam.productmanager.exception.ManagerException;
import com.epam.productmanager.resource.ManagerBundle;


public final class TemplateMap {
	
	private HashMap <String, TemplatesWrapper> templatesMap = new HashMap<String, TemplatesWrapper> ();
	private static final TransformerFactory FACTORY = TransformerFactory.newInstance();
	private static final Logger LOG = Logger.getLogger(TemplateMap.class);
	
	private static final class InstanceHolder {
		private static final TemplateMap instance = new TemplateMap(); 
	}
	
	public static TemplateMap getInstance() {
		return InstanceHolder.instance;
	}
	
	private TemplateMap() {
		
	}
	
	public synchronized Templates getTemplates(String pathName) throws ManagerException {
		TemplatesWrapper wrapper;
		try {
			String path = ManagerBundle.getString(pathName);
			if (templatesMap.containsKey(pathName)) {
				wrapper = templatesMap.get(pathName);
			} else {
				wrapper = new TemplatesWrapper(FACTORY.newTemplates(new StreamSource(ManagerBundle.getString(pathName))), 
						new File(ManagerBundle.getString(pathName)).lastModified());
				templatesMap.put(pathName, wrapper);
			}
			long lastModified = new File(path).lastModified();
			if (lastModified > wrapper.getModified()) {
				wrapper.setTemplates(FACTORY.newTemplates(new StreamSource(path)));
				wrapper.setModified(lastModified);
			}
		} catch (TransformerConfigurationException ex) {
			LOG.error(ex.getMessage());
			throw new ManagerException(ex);
		}
		return wrapper.getTemplates();
	}
}

