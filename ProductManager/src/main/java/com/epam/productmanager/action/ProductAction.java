package com.epam.productmanager.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Formatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import com.epam.productmanager.exception.ManagerException;
import com.epam.productmanager.form.ProductForm;
import com.epam.productmanager.resource.ManagerBundle;
import com.epam.productmanager.resource.ManagerConstants;
import com.epam.productmanager.resource.lock.LockDealer;
import com.epam.productmanager.resource.template.TemplateMap;

public final class ProductAction extends DispatchAction {
	
	private static final String PATH = "/ProductAction.do?method=goods&category=%s&subcategory=%s";
	private static final Logger LOG = Logger.getLogger(ProductAction.class);

	public ActionForward categories(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		ProductForm productForm = (ProductForm) form;
		try {
			reloadDocument(productForm);
		} catch (ManagerException ex) {
			LOG.error(ex.getMessage());
		}
		return mapping.findForward(ManagerConstants.CATEGORIES);
	}
	
	public ActionForward subcategories(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		ProductForm productForm = (ProductForm) form;
		try {
			reloadDocument(productForm);
			String category = request.getParameter(ManagerConstants.CATEGORY);
			request.getSession().setAttribute(ManagerConstants.CATEGORY, category);
			List<Element> elementList = productForm.getDocument().getRootElement().getChildren();
			for (Element element : elementList) {
				if (element.getAttribute(ManagerConstants.NAME).getValue().equals(category)) {
					request.getSession().setAttribute(ManagerConstants.CATEGORY_ID, elementList.indexOf(element));
					break;
				}
			}
		} catch (ManagerException ex) {
			LOG.error(ex.getMessage());
		}
		return mapping.findForward(ManagerConstants.SUBCATEGORIES);
	}
	
	public ActionForward goods(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		ProductForm productForm = (ProductForm) form;
		try {
			reloadDocument(productForm);
			String category = request.getParameter(ManagerConstants.CATEGORY);
			request.getSession().setAttribute(ManagerConstants.CATEGORY, category);
			String subcategory = request.getParameter(ManagerConstants.SUBCATEGORY);
			request.getSession().setAttribute(ManagerConstants.SUBCATEGORY, subcategory);
			List<Element> elementList = productForm.getDocument().getRootElement().getChildren();
			for (Element element : elementList) {
				if (element.getAttribute(ManagerConstants.NAME).getValue().equals(category)) {
					request.getSession().setAttribute(ManagerConstants.CATEGORY_ID, elementList.indexOf(element));
					elementList = element.getChildren();
					break;
				}
			}
			for (Element element : elementList) {
				if (element.getAttribute(ManagerConstants.NAME).getValue().equals(subcategory)) {
					request.getSession().setAttribute(ManagerConstants.SUBCATEGORY_ID, elementList.indexOf(element));
					break;
				}
			}
		} catch (ManagerException ex) {
			LOG.error(ex.getMessage());
		}
		return mapping.findForward(ManagerConstants.GOODS);
	}
	
	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String category = request.getParameter(ManagerConstants.CATEGORY);
		request.getSession().setAttribute(ManagerConstants.CATEGORY, category);
		String subcategory = request.getParameter(ManagerConstants.SUBCATEGORY);
		request.getSession().setAttribute(ManagerConstants.SUBCATEGORY, subcategory);
		return mapping.findForward(ManagerConstants.ADD);
	}
	
	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		Writer stringWriter = null, fileWriter = null, bufferedWriter = null;
		try {
			Templates templates;
			Transformer transformer;
			
			stringWriter = new StringWriter();
			
			templates = TemplateMap.getInstance().getTemplates(ManagerConstants.XSL_SAVE);
			transformer = templates.newTransformer();
			transformer.setParameter(ManagerConstants.CATEGORY, request.getParameter(ManagerConstants.CATEGORY));
			transformer.setParameter(ManagerConstants.SUBCATEGORY, request.getParameter(ManagerConstants.SUBCATEGORY));
			transformer.setParameter(ManagerConstants.PRODUCER, request.getParameter(ManagerConstants.PRODUCER));
			transformer.setParameter(ManagerConstants.MODEL, request.getParameter(ManagerConstants.MODEL));
			transformer.setParameter(ManagerConstants.DATE, request.getParameter(ManagerConstants.DATE));
			transformer.setParameter(ManagerConstants.COLOR, request.getParameter(ManagerConstants.COLOR));
			transformer.setParameter(ManagerConstants.PRICE, request.getParameter(ManagerConstants.PRICE));
			transformer.transform(new StreamSource(ManagerBundle.getString(ManagerConstants.XML_PATH)), new StreamResult(stringWriter));
			
			LockDealer.getReadWriteLock().writeLock().lock();
			try {
				fileWriter = new FileWriter(ManagerBundle.getString(ManagerConstants.XML_PATH));
				bufferedWriter = new BufferedWriter(fileWriter);
				bufferedWriter.write(stringWriter.toString());
			} finally {
				LockDealer.getReadWriteLock().writeLock().unlock();
			}
			
		} catch (IOException | ManagerException | TransformerException ex) {
			LOG.error(ex.getMessage());
		} finally {
			quietWriterClose(stringWriter);
			quietWriterClose(bufferedWriter);
			quietWriterClose(fileWriter);
		}
		
		return new ActionForward(redirectToURL(request.getParameter(ManagerConstants.CATEGORY), request.getParameter(ManagerConstants.SUBCATEGORY)));
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		ProductForm productForm = (ProductForm) form;
		FileWriter writer = null;
		LockDealer.getReadWriteLock().writeLock().lock();
		try {
			writer = new FileWriter(ManagerBundle.getString(ManagerConstants.XML_PATH));
			new XMLOutputter().output(productForm.getDocument(), writer);
		} catch (IOException ex) {
			LOG.error(ex.getMessage());
		} finally {
			LockDealer.getReadWriteLock().writeLock().unlock();
			quietWriterClose(writer);
		}
		return mapping.findForward(ManagerConstants.GOODS);
	}
	
	private void reloadDocument(ProductForm productForm) throws ManagerException {
		LockDealer.getReadWriteLock().readLock().lock();
		try {
			File file = new File(ManagerBundle.getString(ManagerConstants.XML_PATH));
			if (productForm.getLastModified() < file.lastModified()) {
				SAXBuilder builder = new SAXBuilder();
				productForm.setDocument(builder.build(file));
				productForm.setLastModified(file.lastModified());
			}
		} catch(JDOMException | IOException ex) {
			LOG.error(ex.getMessage());
			throw new ManagerException(ex);
		} finally {
			LockDealer.getReadWriteLock().readLock().unlock();
		}
	}
	
	private void quietWriterClose(Writer writer) {
		try {
			if (writer != null) {
				writer.close();
			}
		} catch (IOException ex) {
			LOG.info(ex.getMessage());
		}
	}
	
	@SuppressWarnings("resource")
	private String redirectToURL(String category, String subcategory) {
		return new Formatter().format(PATH, category, subcategory).toString();
	}
}
