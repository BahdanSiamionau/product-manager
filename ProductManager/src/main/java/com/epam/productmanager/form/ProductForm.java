package com.epam.productmanager.form;

import org.apache.struts.action.ActionForm;
import org.jdom2.Document;

@SuppressWarnings("serial")
public final class ProductForm extends ActionForm {
	
	private Document document;
	private long lastModified;

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}
}
