package com.epam.productmanager.exception;

@SuppressWarnings("serial")
public final class ManagerException extends Exception {
	
	public ManagerException() {}
	
	public ManagerException(String msg) {
		super(msg);
	}
	
	public ManagerException(Exception ex) {
		super(ex);
	}
}
